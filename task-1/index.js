const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const PORT = 3000;

app.use(bodyParser.json());

const data = [
	{ name: 'Mov1' },
	{ name: 'Mov2' },
	{ name: 'Mov3' },
	{ name: 'Mov4' }
];

// GET all movies
app.get('/movies', (req, res) => {
	return res.status(200).json(data);
});

// POST a new movie
app.post('/movie', (req, res) => {
	data.push(req.body);
	return res.status(200).json({ message: 'Movie successfully added' });
});

// GET movie by id
app.get('/movie/:id', (req, res) => {
	const id = req.params.id;
	if (id >= data.length) {
		return res.status(404).json({ message: 'Movie not found' });
	}
	return res.status(200).json(data[id]);
});

// GET a random movie
app.get('/movie', (req, res) => {
	const randomId = Math.floor(Math.random() * data.length);
	return res.status(200).json(data[randomId]);
});

app.listen(3000, () => console.log(`Server listening on ${PORT}`));
